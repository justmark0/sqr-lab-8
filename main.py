from selenium import webdriver

# Initialize driver
driver = webdriver.Chrome()

# Open website
driver.get('https://wowlol.ru/achiv/')

# Write name of achievement
username_input = driver.find_element('id', 'title')
username_input.send_keys('name of achivka')

# Write description of achievement
password_input = driver.find_element('id', 'dost')
password_input.send_keys('text of achivka')

# Click to Submit the login
driver.find_element('name', 'go').click()

# Ensure that we added comment
assert 'Прямая ссылка на добавленный вами ачив:' in driver.page_source

# Close the browser
driver.quit()
