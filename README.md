# Lab 8 - Exploratory testing and UI

## Homework Solution

### Test - Generate achievement (SELENIUM TESTED)

| Action                                    | Result                   |
|-------------------------------------------|--------------------------|
| Open Website (https://wowlol.ru/achiv/)   | Website is opened.       |
| Enter Name and description of achievement | fields are filled.       |
| Press generate button                     | You can see achievement. |

### Test - Search

| Action          | Result                                |
|-----------------|---------------------------------------|
| Open Website.   | Website is opened.                    |
| Search "org"    | Search field must be filled           |
| Click "search"  | Site should show more than 0 results. | 

### Test - Send Perl

| Action                                  | Result                                                           |
|-----------------------------------------|------------------------------------------------------------------|
| Open Website.                           | Website is opened.                                               |
| Enter Material name, keywoards and text | fields should be filled.                                         |
| CLick send to review                    | Site should display 'Спасибо, материал отправлен на рассмотрение' | 
